pipeline {
    agent {
        label "dev_node"
    }
    
    parameters {
        string defaultValue: '', description: 'git地址', name: 'GIT_URL', trim: false
        string defaultValue: '', description: '项目名称', name: 'PROJECT_NAME', trim: false
        string defaultValue: '', description: '微服务名称', name: 'FVS_NAME', trim: false
        string defaultValue: '', description: '合并请求发起人', name: 'USER_NAME', trim: false
        string defaultValue: '', description: '合并请求标题', name: 'MERGE_TITLE', trim: false
        string defaultValue: '', description: '合并请求地址', name: 'MERGE_URL', trim: false
    }

    options {
        // 表示保留3次构建历史
        buildDiscarder(logRotator(numToKeepStr: '3'))
    }
    
    environment {
        GIT_URL = "${params.GIT_URL}"
        PROJECT_NAME = "${params.PROJECT_NAME}"
        REGISTRY="registry-vpc.cn-hangzhou.aliyuncs.com/senguo-pro/$PROJECT_NAME"
        VERSION=new Date().format("yyyyMMddHHmmss")
        IMAGE = "$REGISTRY:$VERSION"
        FVS_NAME = "${params.FVS_NAME}"
        USER_NAME = "${params.USER_NAME}"
        MERGE_TITLE = "${params.MERGE_TITLE}"
        MERGE_URL = "${params.MERGE_URL}"
    }
    
    stages {
        stage("检出代码") {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: "master"]], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '5e5ac1ae-36ce-4415-baa1-6479b313822a', url: GIT_URL]]])
            }
        }

        stage("构建镜像") {
            steps {
                script {
                    app = docker.build("${env.REGISTRY}")
                }
            }
        }

        stage("推送到镜像仓库") {
            steps {
                script {
                    docker.withRegistry("https://registry-vpc.cn-hangzhou.aliyuncs.com", "df367806-b831-4798-a15f-0aedc0ef405f") {
  	                    app.push("${env.VERSION}")
                    }
                }
            }
            post {
                success {
                    sh label: "清理镜像", script: "docker rmi ${env.IMAGE}"
                }
            }
        }

        stage("部署到容器集群") {
            steps {
                sh label: "更新微服务镜像", script: "kubectl set image deployment/${env.FVS_NAME} ${env.FVS_NAME}=${env.IMAGE} -n pf-pro"
            }
        }
    }
    post {
	  always {
		  build job: 'fvs-message', parameters: [
			  string(name:'FVS_NAME', value: "${env.FVS_NAME}"),
			  string(name:'CODING_BRANCH', value: "master"),
			  string(name:'USER_NAME', value: "${env.USER_NAME}"),
			  string(name:'MERGE_TITLE', value: "${env.MERGE_TITLE}"),
			  string(name:'BUILD_NUMBER', value: "${env.BUILD_NUMBER}"),
			  string(name:'MERGE_URL', value: "${env.MERGE_URL}")
		  ], wait: false
          cleanWs()
	  }
  }
}
